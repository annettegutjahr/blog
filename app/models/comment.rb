class Comment < ActiveRecord::Base
  belongs_to :post
  validates_presence-of :post_id
  validates_presence-of :body
end
